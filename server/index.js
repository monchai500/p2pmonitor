const express = require("express");
const cors = require("cors");
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const axios = require("axios");
const app = express()
const port = 3001
const _ = require("lodash");
const qs = require("querystring");
const apiClient = async (config, req, res) => {
	try {
		// console.log(config, req, res)

		const client = axios.create({
			transformRequest: [
				(data, headers) => {
					// console.log('checkkkkkk' ,data,headers)
					return data;
				},
				...axios.defaults.transformRequest
			],
			transformResponse: [
				(data) => {
					try {
						// console.log('Check response' ,data)
						return data;
					} catch (error) {
						console.log("error")
						return error
						// console.log(error)
					}
				},
				...axios.defaults.transformResponse
			]
		});

		client.interceptors.request.use(
			config => {
				config.headers = {
					"accept": "*/*",
					"Content-Type": "application/json",
					"Access-Control-Allow-Origin": "*",
					'User-Agent': 'TogTak-HIDDEN-KINGDOM'
				};
				return config;
			},
			error => {
				Promise.reject(error)
			});

		client.interceptors.response.use(
			response => {
				// console.log(response)
				return Promise.resolve(response)
			},
			error => {
				console.log('Check Error ' ,error)
				return Promise.reject(error);
			});
		return client.request(config);
	}catch (e) {
		console.log(e);
	}

};
app.use(cookieParser());
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:3325', credentials: true }));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({
	extended: true,
}));
app.disable('x-powered-by');

app.post("/api", async (req, res,next) => {
	try {
		let body = req.body;
		// console.log(body)
		const response = await apiClient(body, req, res);
		res.status(200).send(response.data)
	}
	catch (err) {
		console.log(err)
	}
})

app.post("/LineNotification", async (req, res,next) => {
	try {
		let body = req.body;
		console.log(req)
		console.log(JSON.parse(body))
		const token = 'NG1BE73qJUTBYGEZjM3bD2qksGwCldvvAi8xvukuDTw'
		if (!token) {
			throw new Error('token is required');
		}
		const options = {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': `Bearer ${token}`,
			},
		};

		let params = {
			message: body,
		}

		let response = await axios.post(`https://notify-api.line.me/api/notify`, qs.stringify(params), options);
		res.status(200).send(response.data)
	}
	catch (err) {
		// console.log(err)
	}
})


app.get('*', (req, res) => {
	res.status(500).json({ message: "error" })
})

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})
