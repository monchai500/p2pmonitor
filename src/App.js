import {Layout, Menu, Breadcrumb, Button} from 'antd';
import React, {useState} from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
} from "react-router-dom";
import GetAll from "./page/GetAll";
import CheckToken from "./page/CheckToken";
import {SetLogin} from "./page/MorningMoon/Login";
import GetItems from "./page/MorningMoon/GetItem";
import AllFunctions from "./page/MorningMoon/AllFunctions";
import CheckPrice from "./page/p2p/CheckPrice";
import Bot from "./page/p2p/bot";

const {Header, Content, Footer} = Layout;
const App = () => {
	let [W3Account, setW3Account] = useState('')
	// const FetchW3Account = async () => {
	// 	let getAccount = await SetLogin()
	// 	setW3Account(getAccount)
	// }
	const routes = [
		{
			path: "/",
			exact: true,
			main: () => <GetAll/>
		},
		{
			path: "/bot",
			exact: true,
			main: () => <Bot/>
		},
		// {
		// 	path: "/CheckToken",
		// 	main: () => <CheckToken/>
		// },
		{
			path: "/CheckPrice",
			main: () => <CheckPrice/>
		},
		// {
		// 	path: "/AllFunctions",
		// 	main: () => <AllFunctions/>
		// },
	];
	return (
		<Router>
			<div className="App">
				<Layout className="layout">
					<Header>
						<div className="logo"/>
						<Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
							<Menu.Item key={1}><Link to="/">Home</Link></Menu.Item>
							<Menu.Item key={2}><Link to="/bot">Bot</Link></Menu.Item>
							{/*<Menu.Item key={2}><Link to="/CheckToken">CheckToken</Link></Menu.Item>*/}
							<Menu.Item key={3}><Link to="/CheckPrice">CheckPrice</Link></Menu.Item>
							{/*<Menu.Item key={4}><Link to="/AllFunctions">AllFunctions</Link></Menu.Item>*/}
							{/*<Menu.Item key={5}><button onClick={() => FetchW3Account()}>Login</button></Menu.Item>*/}
						</Menu>
						<Switch>
							{routes.map((route, index) => (
								<Route
									key={index}
									path={route.path}
									exact={route.exact}
									children={<route/>}
								/>
							))}
						</Switch>
					</Header>
					<Content style={{padding: '0 50px'}}>
						<Breadcrumb style={{margin: '16px 0'}}/>
						<div className="site-layout-content">
							<Switch>
								{routes.map((route, index) => (
									// Render more <Route>s with the same paths as
									// above, but different components this time.
									<Route
										key={index}
										path={route.path}
										exact={route.exact}
										children={<route.main/>}
									/>
								))}
							</Switch>
						</div>
					</Content>
					<Footer style={{textAlign: 'center'}}>Created TogTak</Footer>
				</Layout>
			</div>
		</Router>
	);
}

export default App;
