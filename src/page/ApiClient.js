import axios from "axios";

const apiClient = (config) => {
	// console.log(config)
	const { redirect, ...axiosConfig } = config;
	axiosConfig.withCredentials = true;
	const client = axios.create(axiosConfig);

	client.interceptors.request.use(
		async (config) => {

			if (redirect === true) {
				const body = JSON.parse(JSON.stringify(config));
				// console.log(body)
				delete body.transformRequest;
				delete body.transformResponse;
				config.baseURL = 'http://localhost:3001/';
				config.url = "/api";
				config.method = "post";
				config.data = body
				config.params = null;
			}
			return config;
		},
		(error) => Promise.resolve({ error })
	);

	client.interceptors.response.use(
		(response) => Promise.resolve(response),
		(error) => {
			// Session timeout or wrong RSA key pair
			// if (error.response.status === 440 || error.response.status === 405) {
			//
			// 	// Reload browser window
			// 	window.location.href = "/login";
			//
			// }
			return Promise.reject(error);

		}
	);

	return client;

};

export const GetApiP2P = () => apiClient({
	"redirect": true,
	"baseURL": "https://p2p.binance.com/",
	"headers": {
		"Accept": "application/json",
		"Content-Type": "application/json"
	}
});

export const GetApiTik = () => apiClient({
	"redirect": true,
	"baseURL": "http://tikkubzaza.trueddns.com:54240/",
	"headers": {
		"Accept": "application/json",
		"Content-Type": "application/json"
	}
});

export const GetApiBinance = () => apiClient({
	"redirect": true,
	"baseURL": "https://api.binance.com/api",
	"headers": {
		"Accept": "application/json",
		"Content-Type": "application/json"
	}
});
export const GetApi = (url) => {
	return apiClient({
		"baseURL": url,
		"redirect": true,
		"headers": {
			"Accept": "application/json",
			"Content-Type": "application/json",
		},
	});
};
export const LineNotification = () => apiClient({
	"redirect": false,
	"baseURL": "http://localhost:3001/",
	"headers": {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Authorization': `Bearer NG1BE73qJUTBYGEZjM3bD2qksGwCldvvAi8xvukuDTw`,
	}
});

export const getRate = async () => {
	let rate =  await GetApi('https://www.binance.com/').get(`bapi/asset/v1/public/asset-service/product/currency`)

	console.log(rate)
	return rate
};

