import {Typography, Table} from 'antd';
import {useEffect, useState} from "react";
import Binance from "node-binance-api";
import number from "numeral";
import _ from "lodash";
import {GetApi} from "./ApiClient";

const {Text} = Typography;
const CheckToken = () => {
	let [ListCoin, setListCoin] = useState([])
	let [Rate, setRate] = useState('')
	const factDate = async () => {
		let listToken = [
			{coin:'0x50332bdca94673f33401776365b66cc4e81ac81d'},
			{coin:'0x31471e0791fcdbe82fbf4c44943255e923f1b794'},
			{coin:'0x3581a7b7be2ed2edf98710910fd05b0e8545f1db'},
			{coin:'0x35754e4650c8ab582f4e2cb9225e77e6685be25a'},
			{coin:'0xd9025e25bb6cf39f8c926a704039d2dd51088063'},
			{coin:'0xa6c897caaca3db7fd6e2d2ce1a00744f40ab87bb'},
			{coin:'0x68e374f856bf25468d365e539b700b648bf94b67'},
		]
		let result = [];
		for (const {coin} of listToken) {
			const {data} = await GetApi('https://api.pancakeswap.info/api/').get(`v2/tokens/${coin}`);
			result.push(data.data)
		}
		return result
	}
	useEffect(() => {
		const interval = setInterval(async () => {
			const Coins = await factDate();
			setListCoin(Coins)
		}, 5000);
		return () => {
			clearInterval(interval)
		}
	}, []);
	const columns = [
		{
			title: 'name',
			dataIndex: 'name',
			render: (record) => {
				return <div><Text style={{fontSize: '20px'}} level={1} >{record}</Text></div>
			}
		},
		{
			title: 'price',
			dataIndex: 'price',
			render: (record) => {
				return <div><Text style={{fontSize: '20px'}} level={1} >{number(record).format('0.0000000')}</Text></div>
			}
		},
		{
			title: 'price_BNB',
			dataIndex: 'price_BNB',
			render: (record) => {
				return <div><Text style={{fontSize: '20px'}} level={1} >{number(record).format('0.0000000')}</Text></div>
			}
		},
		{
			title: 'symbol',
			dataIndex: 'symbol',
		},
	];
	const onChange = (pagination, filters, sorter, extra) => {
		console.log('params', pagination, filters, sorter, extra);
	}
	return <>
		<Table columns={columns} dataSource={ListCoin} pagination={{total: 200}} onChange={onChange}/>
	</>
}
export default CheckToken;
