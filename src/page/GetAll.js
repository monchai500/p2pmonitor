import {Typography, Table, Form, Button, Input} from 'antd';
import {useEffect, useState} from "react";
import Binance from "node-binance-api";
import number from "numeral";
import _ from "lodash";
import {GetApi, getRate, LineNotification} from "./ApiClient";
const {Text} = Typography;
const InputPrice = ({Price}) => {
	return <>
		<Form
			name="customized_form_controls"
			layout="inline"
			onFinish={(val) => Price(val.price)}
		>
			<Form.Item name="price" label="Price" >
				<Input/>
			</Form.Item>
			<Form.Item>
				<Button type="primary" htmlType="submit">
					Submit
				</Button>
			</Form.Item>
		</Form>
	</>
}
const GetAll = () => {
	let [ListCoin, setListCoin] = useState([])
	let [Rate, setRate] = useState('')
	let [RateFee, setRateFee] = useState([])
	let [Price, setPrice] = useState('')
	const binance = new Binance().options({
		APIKEY: 'MLOXE5XGD3IS9WBbn6bN7xBdnJplfb4tvek53wFpYoP7yn4odiTnkJDPOnfIyBuv',
		APISECRET: 'kquQBlRXjUjNrWSWxCrscWruhkSzvHOnVc6TDX8mu2QpfIIshpZSIs6o7jun0JTI',
	});
	useEffect(() => {
		GetApi('https://www.binance.com/').get(`bapi/asset/v1/public/asset-service/product/currency`).then(result => {
			let [rateThb] = _.filter(result.data.data, {'pair': 'THB_USD'});
			setRate(rateThb.rate)
		})
		const interval = setInterval(async () => {
			let rateData = await GetApi('https://www.binance.com/').get(`bapi/asset/v1/public/asset-service/product/currency`);
			let [rateThb] = _.filter(rateData.data.data, {'pair': 'THB_USD'});
			let rate = rateThb.rate
			setRate(rate)
		}, 30000);
		return () => {
			clearInterval(interval)
		}
	}, [Rate]);
	useEffect(() => {
		const interval = setInterval(async () => {
			let list = [
				{key: 'USDT'},
				{key: 'ADA'},
				{key: 'BNB'},
				{key: 'SCRT'},
				{key: 'IOST'},
				{key: 'SAND'},
				{key: 'XLM'},
				{key: 'NEAR'},
				{key: 'XRP'},
				{key: 'LTC'},
				{key: 'DOGE'},
				{key: 'ZIL'},
				{key: 'MANA'},
				{key: 'BUSD'},
			]
			let dataBinance = await binance.bookTickers()
			let dataSatang = await GetApi('https://satangcorp.com').get(`/api/v3/ticker/24hr`);
			let dataBitkub = await GetApi('https://api.bitkub.com').get(`/api/market/ticker`);
			let result = [];
			for (const {key} of list) {
				let coin = key + "BUSD";
				let newcoin = dataBinance[coin] ? dataBinance[coin] : dataBinance[key + 'USDT'] + key + 'USDT'
				let recordBitkub = dataBitkub.data[`THB_${key}`] ? dataBitkub.data[`THB_${key}`] :{highestBid:0,lowestAsk:0}
				let recordSatang = dataSatang.data.some(symbol => symbol.symbol === `${key.toLowerCase()}_thb`) === true ? dataSatang.data.find(symbol => symbol.symbol === `${key.toLowerCase()}_thb`) : false
				result.push({
					Coins: coin,
					Binance: {buy: (newcoin.bid * Rate), sell: (newcoin.ask * Rate)},
					Diffs: {
						buy: (recordBitkub.highestBid) - (newcoin.bid * Rate),
						sell: (recordBitkub.lowestAsk) - (newcoin.ask * Rate)
					},
					Bitkub: {buy: (recordBitkub.highestBid), sell: (recordBitkub.lowestAsk)},
					Satang: recordSatang ? {buy:recordSatang.bidPrice,sell:recordSatang.askPrice} : {buy:0,sell:0},
					key: key,
					rate: {binance: (newcoin.bid), bitkub: (recordBitkub.highestBid)}
				})
			}
			setListCoin(result)
		}, 3000);
		return () => {
			clearInterval(interval)
		}
	}, [Price]);
	const columns = [
		{
			title: 'Coins',
			dataIndex: 'Coins',
			width: 100
		},
		{
			title: `Binance ==> `,
			width: 350,
			render: ({Bitkub, rate, Binance, key}) => {
				let price = ((( Price - (Price * 0.10)/100) / Binance.sell) * Bitkub.buy) - Price;
				// let amount = 99900/ Binance.sell;
				return <div><Text style={{fontSize: '20px'}} level={2}
								  type={price <= 0 ? 'danger' : 'success'}>{number(price).format('฿0,0.00')}</Text>
				</div>
			}
		},
		{
			title: `<== Bitkub`,
			width: 350,
			render: ({Bitkub, rate, Binance, key}) => {
				let price = ((( Price - (Price * 0.25)/100) / Bitkub.sell) * Binance.buy) - Price;
				// let amount = 99750 / Bitkub.sell;
				return <div><Text style={{fontSize: '20px'}} level={1}
								  type={price <= 0 ? 'danger' : 'success'}>{number(price).format('฿0,0.00')}</Text>
				</div>
			}
		},
		{
			title: 'Binance (' + Rate + ')',
			dataIndex: 'Binance',
			width: 350,
			render: (record) => {
				return number(record.buy).format('฿0,0.000') + " / " + number(record.sell).format('฿0,0.000')
			}
		},
		// {
		// 	title: 'Diffs',
		// 	dataIndex: 'Diffs',
		// 	width: 350,
		// 	render: (record) => {
		// 		let buy = record.buy;
		// 		let sell = record.sell
		// 		return (<div>
		// 			Buy : <Text level={2} type={buy <= 0 ? 'danger' : 'success'}>{number(buy).format('฿0,0.000')}</Text>
		// 			Sell : <Text level={2}
		// 						 type={sell <= 0 ? 'danger' : 'success'}>{number(sell).format('฿0,0.000')}</Text>
		// 		</div>)
		// 	}
		// },
		{
			title: 'Bitkub',
			dataIndex: 'Bitkub',
			width: 350,
			render: (record) => {
				return number(record.buy).format('฿0,0.000') + " / " + number(record.sell).format('฿0,0.000')
			}
		},
		{
			title: 'Satang',
			dataIndex: 'Satang',
			width: 350,
			render: (record) => {
				return number(record.buy).format('฿0,0.000') + " / " + number(record.sell).format('฿0,0.000')
			}
		},
		{
			title: `Binance ==>`,
			width: 350,
			render: ({Satang, rate, Binance, key}) => {
				let price = ((( Price - (Price * 0.10)/100) / Binance.sell) * Satang.buy) - Price;
				// let amount = 99900/ Binance.sell;
				return <div><Text style={{fontSize: '20px'}} level={2}
								  type={price <= 0 ? 'danger' : 'success'}>{number(price).format('฿0,0.00')}</Text>
				</div>
			}
		},
		{
			title: `<== Satang`,
			width: 350,
			render: ({Satang, rate, Binance, key}) => {
				let price = ((( Price - (Price * 0.2)/100) / Satang.sell) * Binance.buy) - Price;
				// let amount = 99750 / Satang.sell;
				return <div><Text style={{fontSize: '20px'}} level={1}
								  type={price <= 0 ? 'danger' : 'success'}>{number(price).format('฿0,0.00')}</Text>
				</div>
			}
		},

	];
	return <>
		<InputPrice Price={(val) => setPrice(val) }/>
		<Table columns={columns} dataSource={ListCoin}/>
	</>
}
export default GetAll;
