import {Typography, Table, Form, Button, Input, Card, Col, Row, Switch} from 'antd';
import {useEffect, useState} from "react";
import number from "numeral";
import _ from "lodash";
import {GetApi, GetApiBinance, GetApiP2P, LineNotification} from "../ApiClient";

const {Text} = Typography;
const InputPrice = ({CenterRate,BastRate,FastRate,LastSell}) => {
	const [AutoUSDT, setAutoUSDT] = useState(false)
	const [AutoBUSD, setAutoBUSD] = useState(false)
	const [FormUSDT] = Form.useForm();
	const [FormBUSD] = Form.useForm();
	const [ResultUSDT, setResultUSDT] = useState({price: 0, amount: 0, swap: 0,swapFast: 0, bastSell: 0, fastSell: 0})
	const [ResultBUSD, setResultBUSD] = useState({price: 0, amount: 0, swap: 0,swapFast: 0, bastSell: 0, fastSell: 0})
	useEffect(() => {
		(async () => {
			let CheckAutoUSDT;
			let CheckAutoBUSD;
			if(AutoUSDT){
				CheckAutoUSDT = {
					swap: CenterRate.USDT,
					bastSell:BastRate.USDT,
					fastSell:FastRate.USDT,
					price:LastSell.USDT,
				}
			}
			if(AutoBUSD){
				CheckAutoBUSD = {
					swap: CenterRate.BUSD,
					bastSell:BastRate.BUSD,
					fastSell:FastRate.BUSD,
					price:LastSell.BUSD,
				}
			}
			FormUSDT.setFieldsValue({
				...CheckAutoUSDT,
				type: 'USDT'
			});
			FormBUSD.setFieldsValue({
				...CheckAutoBUSD,
				type: 'BUSD'
			});
		})()
	}, [CenterRate,AutoBUSD]);
	const onFinished = (value) => {

		let sumFee = value.amount - ((value.amount * 0.2) / 100) // 34.32 ซื้อเหรียนจำนวน 6000 -0.2% เหลือ 5988 ทุน 5988 205,508 ทุนที่จ่ายจริงๆ 205,920
		let sumBnb = sumFee / value.swap // จำนวน BNB ยังไม่หักค่าคอม ได้ bnb  10.1681
		let sumSwapFee = sumBnb - ((sumBnb * 0.075) / 100) // จำนวน BNB หลังจาก หักค่าคอม 10.1604

		let sumBastlSell = (sumSwapFee * value.bastSell)  // คือ เอา BNB คูณกับ ราคาขายแพง 10.1604 * 20372.00 = 206,987
		let totalFastSell = (sumSwapFee * value.fastSell) // เอา bnb ทั้งหมดที่แปลงจาก ทุนแล้วไปขาย คนตั้งรับ คนแรก เพื่อทำกำไรเลย 204,348

		let totalBastSellFee = sumBastlSell - ((sumBastlSell * 0.2) / 100) // ค่าคอมหลังตั้งร้านขายได้ 206,575 Net

		let tun = value.price * value.amount // เงินที่จ่ายจริงสุธิ 205,920

		let totalBast = (totalBastSellFee - tun)
		let totalFast = (totalFastSell - tun)

		let result = {price: sumSwapFee, amount: tun, swap: totalBastSellFee,swapFast: totalFastSell, bastSell: totalBast, fastSell: totalFast}

		// setAutoUSDT(value.auto)
		if (value.type === 'USDT') {
			setAutoUSDT(value.auto)
			setResultUSDT(result)
		} else {
			setAutoBUSD(value.auto)
			setResultBUSD(result)
		}

	}
	return <>
		<div className="site-card-wrapper">
			<Row gutter={[16, 24]}>
				<Col span={6}>
					<Card title="Form USDT" bordered={true}>
						<Form
							form={FormUSDT}
							name="FormUSDT"
							labelCol={{span: 10}}
							wrapperCol={{span: 14}}
							onFinish={onFinished}
						>
							<Form.Item name="amount" label="จำนวนที่ซื้อ">
								<Input/>
							</Form.Item>
							<Form.Item name="price" label="USDT">
								<Input/>
							</Form.Item>
							<Form.Item name="swap" label="BNB/USDT">
								<Input/>
							</Form.Item>
							<Form.Item name="bastSell" label="ราคาที่จะขาย">
								<Input/>
							</Form.Item>
							<Form.Item name="fastSell" label="ราคาขายเร็ว">
								<Input/>
							</Form.Item>
							<Form.Item name="type" label="type" hidden="true">
								<Input/>
							</Form.Item>
							<Form.Item name="rang" label="อันดับ">
								<Input/>
							</Form.Item>
							<Form.Item label="Auto" name="auto" valuePropName="checked">
								<Switch />
							</Form.Item>
							<Form.Item>
								<Button type="primary" htmlType="submit">
									Submit
								</Button>
							</Form.Item>
						</Form>
					</Card>
				</Col>
				<Col span={6}>
					<Card title="Form USDT" bordered={true}>
						<Form.Item name="price" label="BNB">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultUSDT.price).format('0.00000000')}</Text>
						</Form.Item>
						<Form.Item name="amount" label="ทุนเงินบาท">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultUSDT.amount).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapBast">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultUSDT.swap).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapFast">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultUSDT.swapFast).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="bastSell" label="ขายราคาดีได้กำไร">
							<Text style={{fontSize: '18px'}} type={ResultUSDT.bastSell <= 0 ? 'danger' : 'success'}  level={3}>{number(ResultUSDT.bastSell).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="fastSell" label="ขายราคาเร็วได้กำไร">
							<Text style={{fontSize: '18px'}} type={ResultUSDT.fastSell <= 0 ? 'danger' : 'success'} level={3}>{number(ResultUSDT.fastSell).format('฿0,0.00')}</Text>
						</Form.Item>
					</Card>
				</Col>
				<Col span={6}>
					<Card title="Form BUSD" bordered={true}>
						<Form
							form={FormBUSD}
							name="FormBUSD"
							labelCol={{span: 10}}
							wrapperCol={{span: 14}}
							onFinish={onFinished}
						>
							<Form.Item name="amount" label="จำนวนที่ซื้อ">
								<Input/>
							</Form.Item>
							<Form.Item name="price" label="BUSD">
								<Input/>
							</Form.Item>
							<Form.Item name="swap" label="BNB/BUSD">
								<Input/>
							</Form.Item>
							<Form.Item name="bastSell" label="ราคาที่จะขาย">
								<Input/>
							</Form.Item>
							<Form.Item name="fastSell" label="ราคาขายเร็ว">
								<Input/>
							</Form.Item>
							<Form.Item name="type" label="type" hidden="true">
								<Input/>
							</Form.Item>
							<Form.Item name="rang" label="อันดับ">
								<Input/>
							</Form.Item>
							<Form.Item label="Auto" name="auto" >
								<Switch/>
							</Form.Item>
							<Form.Item>
								<Button type="primary" htmlType="submit">
									Submit
								</Button>
							</Form.Item>
						</Form>
					</Card>
				</Col>
				<Col span={6}>
					<Card title="Form BUSD" bordered={true}>
						<Form.Item name="price" label="BNB">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultBUSD.price).format('0.00000000')}</Text>
						</Form.Item>
						<Form.Item name="amount" label="ทุนเงินบาท">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultBUSD.amount).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapBast">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultBUSD.swap).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapFast">
							<Text style={{fontSize: '18px'}} level={3}>{number(ResultBUSD.swapFast).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="bastSell" label="ขายราคาดีได้กำไร">
							<Text style={{fontSize: '18px'}} type={ResultBUSD.bastSell <= 0 ? 'danger' : 'success'}  level={3}>{number(ResultBUSD.bastSell).format('฿0,0.00')}</Text>
						</Form.Item>
						<Form.Item name="fastSell" label="ขายราคาเร็วได้กำไร">
							<Text style={{fontSize: '18px'}} type={ResultBUSD.fastSell <= 0 ? 'danger' : 'success'} level={3}>{number(ResultBUSD.fastSell).format('฿0,0.00')}</Text>
						</Form.Item>
					</Card>
				</Col>
			</Row>
		</div>
	</>
}
const CheckPrice = () => {
	const [ListCoin, setListCoin] = useState([])
	const [CenterRate, setCenterRate] = useState({USDT: 0, BUSD: 0})
	const [BastRate, setBastRate] = useState({USDT: 0, BUSD: 0})
	const [FastRate, setFastRate] = useState({USDT: 0, BUSD: 0})
	const [LastSell, setLastSell] = useState({USDT: 0, BUSD: 0})

	useEffect(() => {
		const interval = setInterval(async () => {
			let key = 20
			let price = 10
			// const line = await LineNotification().post(`/LineNotification`,{message: 'test'});
			// console.log(line)
			let rateData = await GetApi('https://www.binance.com/bapi/asset/').get(`v1/public/asset-service/product/currency`);
			let [rateThb] = _.filter(rateData.data.data, {'pair': 'THB_USD'});
			let Rate = rateThb.rate
			const getPriceUSDT = await GetApiBinance().get(`/v3/ticker/price`, {params: {symbol: 'BNBUSDT'}});
			const getPriceBUSD = await GetApiBinance().get(`/v3/ticker/price`, {params: {symbol: 'BNBBUSD'}});
			const p2pPrice_SELL_USDT = await GetApiP2P().post('/bapi/c2c/v2/friendly/c2c/adv/search', {
				page: 1,
				fiat: 'THB',
				tradeType: 'SELL',
				asset: "USDT",
				rows: key,
				publisherType: "merchant",
				merchantCheck: true
			});
			const p2pPrice_SELL_BUSD = await GetApiP2P().post('/bapi/c2c/v2/friendly/c2c/adv/search', {
				page: 1,
				fiat: 'THB',
				tradeType: 'SELL',
				asset: "BUSD",
				rows: key,
				publisherType: "merchant",
				merchantCheck: true
			});
			const p2pPrice_SELL_BNB = await GetApiP2P().post('/bapi/c2c/v2/friendly/c2c/adv/search', {
				page: 1,
				fiat: 'THB',
				tradeType: 'SELL',
				asset: "BNB",
				rows: 1,
				publisherType: "merchant",
				merchantCheck: true
			});
			const p2pPrice_BUY_BNB = await GetApiP2P().post('/bapi/c2c/v2/friendly/c2c/adv/search', {
				page: 1,
				fiat: 'THB',
				tradeType: 'BUY',
				asset: "BNB",
				rows: 1,
				publisherType: "merchant",
				merchantCheck: true
			});

			let result = []
			let bastUsdt = p2pPrice_BUY_BNB.data.data[0].adv.price
			let bastBusd = p2pPrice_BUY_BNB.data.data[0].adv.price
			let fastUsdt = p2pPrice_SELL_BNB.data.data[0].adv.price
			let fastBusd = p2pPrice_SELL_BNB.data.data[0].adv.price
			let lastSellUsdt = p2pPrice_SELL_USDT.data.data[0].adv.price
			let lastSellBusd = p2pPrice_SELL_BUSD.data.data[0].adv.price

			setCenterRate({USDT: getPriceUSDT.data.price,BUSD: getPriceBUSD.data.price})
			setBastRate({USDT: bastUsdt,BUSD: bastBusd})
			setFastRate({USDT: fastUsdt,BUSD: fastBusd})
			setLastSell({USDT: lastSellUsdt,BUSD: lastSellBusd})
			//
			//
			// if(bastUsdt > price || bastBusd > price || fastUsdt > price || fastBusd > price){
			// 	await LineNotification().post(`/LineNotification`,`bastUsdt : ${bastUsdt} bastBusd: ${bastBusd} fastUsdt: ${fastUsdt} fastBusd: ${fastBusd}`);
			// }
			const blacklist = ['s2ab631aee8433703867097c356ac7e49']
			for (let i = 0; i < key; i++) {
				// console.log(p2pPrice_SELL_USDT.data.data[i].advertiser)
				if(!blacklist.includes(p2pPrice_SELL_USDT.data.data[i].advertiser.userNo)){
					result.push({
						getPriceUSDT: p2pPrice_BUY_BNB.data.data[0].adv.price - (p2pPrice_SELL_USDT.data.data[i].adv.price * getPriceUSDT.data.price),
						getPriceBUSD: p2pPrice_BUY_BNB.data.data[0].adv.price - (p2pPrice_SELL_BUSD.data.data[i].adv.price * getPriceBUSD.data.price),
						getPriceCenterUSDT: getPriceUSDT.data.price,
						getPriceCenterBUSD: getPriceBUSD.data.price,
						tunUSDT: (getPriceUSDT.data.price * p2pPrice_SELL_USDT.data.data[i].adv.price) - p2pPrice_SELL_USDT.data.data[i].adv.price,
						tunBUSD: (getPriceBUSD.data.price * p2pPrice_SELL_BUSD.data.data[i].adv.price) - p2pPrice_SELL_BUSD.data.data[i].adv.price,
						p2pPrice_SELL_BNB: {
							price: p2pPrice_SELL_BNB.data.data[0].adv.price,
							name: p2pPrice_SELL_BNB.data.data[0].advertiser.nickName
						},
						p2pPrice_BUY_BNB: {
							price: p2pPrice_BUY_BNB.data.data[0].adv.price,
							name: p2pPrice_BUY_BNB.data.data[0].advertiser.nickName
						},
						LastSellUSDT: {
							price: p2pPrice_SELL_USDT.data.data[i].adv.price,
							name: p2pPrice_SELL_USDT.data.data[i].advertiser.nickName
						},
						LastSellBUSD: {
							price: p2pPrice_SELL_BUSD.data.data[i].adv.price,
							name: p2pPrice_SELL_BUSD.data.data[i].advertiser.nickName
						},
						FastSellUSDT: p2pPrice_SELL_BNB.data.data[0].adv.price - (p2pPrice_SELL_USDT.data.data[i].adv.price * getPriceUSDT.data.price),
						FastSellBUSD: p2pPrice_SELL_BNB.data.data[0].adv.price - (p2pPrice_SELL_BUSD.data.data[i].adv.price * getPriceBUSD.data.price)
					})
				}


			}
			console.log(result)
			setListCoin(result)
		}, 1500);
		return () => {
			clearInterval(interval)
		}
	}, []);
	const columns = [
		{
			title: 'กำไรจาก USDT',
			render: ({tunUSDT,p2pPrice_SELL_BNB}) => {

				let result = tunUSDT - p2pPrice_SELL_BNB.price
				return <Text style={{fontSize: '20px'}} level={2}
							 type={result <= 0 ? 'danger' : 'success'}>{number(result).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'กำไรจาก BUSD',
			render: ({tunBUSD,p2pPrice_SELL_BNB}) => {
				// let comFee =  p2pPrice_SELL_BNB.price - ((p2pPrice_SELL_BNB.price * 0.275) / 100)
				let result = tunBUSD - p2pPrice_SELL_BNB.price
				return <Text style={{fontSize: '20px'}} level={2}
							 type={result <= 0 ? 'danger' : 'success'}>{number(result).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'ทุน USDT',
			dataIndex: 'tunUSDT',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'ทุน BUSD',
			dataIndex: 'tunBUSD',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'ราคากลาง USDT',
			dataIndex: 'getPriceCenterUSDT',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'ราคากลาง BUSD',
			dataIndex: 'getPriceCenterBUSD',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'ราคารับซื้อล่าสุด',
			dataIndex: 'p2pPrice_BUY_BNB',
			render: (record) => {
				return <Text style={{fontSize: '20px'}}
							 level={3}>{number(record.price).format('฿0,0.00')} {record.name}</Text>
			},
			width: 200
		},
		{
			title: 'ราคาขายล่าสุด',
			dataIndex: 'p2pPrice_SELL_BNB',
			render: (record) => {
				return <Text style={{fontSize: '20px'}}
							 level={3}>{number(record.price).format('฿0,0.00')} {record.name}</Text>
			},
			width: 200
		},
		{
			title: 'ราคา USDT ล่าสุด',
			dataIndex: 'LastSellUSDT',
			render: (record) => {
				return <Text style={{fontSize: '18px'}}
							 level={3}>{number(record.price).format('฿0,0.00')} {record.name}</Text>
			},
			width: 200
		},
		{
			title: 'ราคา BUSD ล่าสุด',
			dataIndex: 'LastSellBUSD',
			render: (record) => {
				return <Text style={{fontSize: '18px'}}
							 level={3}>{number(record.price).format('฿0,0.00')} {record.name}</Text>
			},
			width: 200
		},
		{
			title: 'กำไรจากการขายไว USDT',
			dataIndex: 'FastSellUSDT',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}
							 type={record <= 0 ? 'danger' : 'success'}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
		{
			title: 'กำไรจากการขายไว BUSD',
			dataIndex: 'FastSellBUSD',
			render: (record) => {
				return <Text style={{fontSize: '20px'}} level={2}
							 type={record <= 0 ? 'danger' : 'success'}>{number(record).format('฿0,0.00')}</Text>
			},
			width: 100
		},
	];
	return <>
		<InputPrice CenterRate={CenterRate} BastRate={BastRate} FastRate={FastRate} LastSell={LastSell} />
		<Table columns={columns} dataSource={ListCoin}/>
	</>
}
export default CheckPrice;
