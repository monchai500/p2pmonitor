import {Typography, Table, Form, Button, Input, Card, Col, Row, Switch} from 'antd';
import {useEffect, useState} from "react";
import number from "numeral";
import _ from "lodash";
import {GetApi, GetApiBinance, GetApiP2P, LineNotification} from "../ApiClient";

const {Text} = Typography;

const Bot = () => {
	const [AutoUSDT, setAutoUSDT] = useState(false)
	const [ListCoin, setListCoin] = useState([])
	const [UpdateStatus, setUpdateStatus] = useState('Pending')
	const [FormUSDT] = Form.useForm();
	useEffect(() => {
		(async () => {
			let rateData = await GetApi('http://tikkubzaza.trueddns.com:54240/').get(`getOrderLists`);
			let result = Object.keys(rateData.data).map(record => {
				return rateData.data[record][0]
			})
			setListCoin(result)
		})()
	}, [UpdateStatus]);

	const onUpdate = async (value) => {
		console.log(value.orderStatus)
		if(value.orderStatus === 1){
			value.orderStatus = 'Cancel'
			setUpdateStatus(value.orderStatus)
		}else{
			value.orderStatus = 'Update'
			setUpdateStatus(value.orderStatus)
		}
	}
	const columns = [
		{title: "orderNumber", dataIndex: "orderNumber"},
		{title: "advOrderId", dataIndex: "advOrderId"},
		{title: "advOrderNumber", dataIndex: "advOrderNumber"},
		{title: "buyerMobilePhone", dataIndex: "buyerMobilePhone"},
		{title: "sellerName", dataIndex: "sellerName"},
		{title: "buyerNickname", dataIndex: "buyerNickname"},
		{title: "Button",render:({orderNumber,orderStatus}) =>  <Button onClick={() => onUpdate({orderNumber, orderStatus})}>{UpdateStatus}</Button>}
	];


	const fateData = async () => {
		let rateData = await GetApi('http://tikkubzaza.trueddns.com:54240/').get(`getOrderLists`);
		console.log(rateData.data.TIK)
		return {data:rateData.data.TIK}
	}
	return <>
		<div className="site-card-wrapper">
			<Row gutter={[16, 24]}>
				<Col span={6}>
					<Card title="Form USDT" bordered={true}>
						<Form
								form={FormUSDT}
								name="FormUSDT"
								labelCol={{span: 10}}
								wrapperCol={{span: 14}}
						>
							<Form.Item name="amount" label="จำนวนที่ซื้อ">
								<Input/>
							</Form.Item>
							<Form.Item name="price" label="USDT">
								<Input/>
							</Form.Item>
							<Form.Item name="swap" label="BNB/USDT">
								<Input/>
							</Form.Item>
							<Form.Item name="bastSell" label="ราคาที่จะขาย">
								<Input/>
							</Form.Item>
							<Form.Item name="fastSell" label="ราคาขายเร็ว">
								<Input/>
							</Form.Item>
							<Form.Item name="type" label="type" hidden="true">
								<Input/>
							</Form.Item>
							<Form.Item name="rang" label="อันดับ">
								<Input/>
							</Form.Item>
							<Form.Item label="Auto" name="auto" valuePropName="checked">
								<Switch/>
							</Form.Item>
							<Form.Item>
								<Button type="primary" htmlType="submit">
									Submit
								</Button>
							</Form.Item>
						</Form>
					</Card>
				</Col>
				<Col span={6}>
					<Card title="Form USDT" bordered={true}>
						<Form.Item name="price" label="BNB">
							<Text style={{fontSize: '18px'}} level={3}>เทส</Text>
						</Form.Item>
						<Form.Item name="amount" label="ทุนเงินบาท">
							<Text style={{fontSize: '18px'}} level={3}>เทส</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapBast">
							<Text style={{fontSize: '18px'}} level={3}>เทส</Text>
						</Form.Item>
						<Form.Item name="swap" label="หลังSwapFast">
							<Text style={{fontSize: '18px'}} level={3}>เทส</Text>
						</Form.Item>
						<Form.Item name="bastSell" label="ขายราคาดีได้กำไร">
							<Text style={{fontSize: '18px'}} type={'danger'} level={3}>เทส</Text>
						</Form.Item>
						<Form.Item name="fastSell" label="ขายราคาเร็วได้กำไร">
							<Text style={{fontSize: '18px'}} type={'danger'} level={3}>เทส</Text>
						</Form.Item>
					</Card>
				</Col>
			</Row>
			<Row>
				<Table columns={columns} dataSource={ListCoin}/>
			</Row>
		</div>
	</>
}

export default Bot;
